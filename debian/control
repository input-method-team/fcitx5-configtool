Source: fcitx5-configtool
Priority: optional
Section: kde
Maintainer: Debian Input Method Team <debian-input-method@lists.debian.org>
Uploaders:
 Boyuan Yang <byang@debian.org>,
 Shengjing Zhu <zhsj@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 extra-cmake-modules,
 gettext,
 iso-codes,
 libfcitx5-qt6-dev (>= 5.1),
 libfcitx5core-dev (>= 5.1),
 libfcitx5utils-dev (>= 5.1),
 libkf6coreaddons-dev,
 libkf6dbusaddons-dev,
 libkf6declarative-dev,
 libkf6i18n-dev,
 libkf6iconthemes-dev,
 libkf6itemviews-dev,
 libkf6kcmutils-dev,
 libkf6package-dev,
 libkf6svg-dev,
 libkf6widgetsaddons-dev,
 libkf6windowsystem-dev,
 libkirigami-dev,
 libplasma-dev,
 libxkbcommon-dev,
 libxkbfile-dev,
 pkgconf,
 qt6-base-dev (>= 6.5),
 qt6-declarative-dev,
 xkb-data,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/fcitx/fcitx5-configtool
Vcs-Git: https://salsa.debian.org/input-method-team/fcitx5-configtool.git
Vcs-Browser: https://salsa.debian.org/input-method-team/fcitx5-configtool

Package: fcitx5-config-qt
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 fcitx5-configtool-data (<< 5.0.15),
 kde-config-fcitx5 (<< 0.0~git20200822),
Replaces:
 fcitx5-configtool-data (<< 5.0.15),
 kde-config-fcitx5 (<< 0.0~git20200822),
Provides:
 fcitx5-configtool,
Recommends:
 libime-bin,
Suggests:
 kde-config-fcitx5,
Description: configuration tool for Fcitx5 (Qt version)
 This is a configuration tool for Fcitx Input Method Framework v5.
 .
 It provides several tools:
  - fcitx5-config-qt.
  - kbd-layout-viewer5.
  - fcitx5-migrator, which helps to migrate data from Fcitx4.
    To migrate Fcitx4 pinyin and table data, it needs tools from
    libime-bin package.

Package: kde-config-fcitx5
Architecture: any
Depends:
 kde-cli-tools,
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 fcitx5-configtool-kde (<< 5.0.15),
Replaces:
 fcitx5-configtool-kde (<< 5.0.15),
Suggests:
 fcitx5-config-qt,
Description: KDE configuration module for Fcitx5
 This is a configuration module for System Settings for configuring
 Fcitx Input Method Framework v5. It also provides the
 fcitx5-plasma-theme-generator tool.
